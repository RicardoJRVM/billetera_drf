from django.db import models

# Create your models here.
class Cuenta(models.Model):
    id_cuenta = models.CharField(max_length=15)
    nombre_titular = models.CharField(max_length=100)
    apellido_titular = models.CharField(max_length=100)
    saldo = models.DecimalField(max_digits=10, decimal_places=2)