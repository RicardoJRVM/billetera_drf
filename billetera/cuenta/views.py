from django.shortcuts import render
from rest_framework import viewsets
from .serializers import CuentaSerializer
from .models import Cuenta
from rest_framework.response import Response
from django.db import transaction

# Create your views here.
class CuentaViewSet(viewsets.ModelViewSet):
    queryset = Cuenta.objects.all()
    serializer_class = CuentaSerializer

    def list(self,request,*args ,**kwarg):
        with transaction.atomic():
            operacion = Cuenta.objects.all()
            serializer = CuentaSerializer(operacion,many=True)
            return Response(serializer.data)

    def create(self,request,*arg, **kwargs):
        with transaction.atomic():
            try:
                #Guardado normal
                serializer = CuentaSerializer(data=request.data)
                serializer.is_valid(raise_exception=True)
                serializer.save()

                #Crear un objecto incompleto para que falle
                failure = Cuenta.objects.create(id_cuenta=111)

                #Mensaje de respuesta
                serializerResponse = serializer.data
                return Response(serializerResponse)
            except Exception as e:
                print(e)
                return Response({})

    def update(self,request,*args,**kwargs):
        instance = self.get_object()
        instance.id_cuenta = request.data.get("id_cuenta")
        instance.nombre_titular = request.data.get("nombre_titular")
        instance.apellido_titular = request.data.get("apellido_titular")
        instance.saldo = request.data.get("saldo")
        instance.save()
        serializer = CuentaSerializer(data=instance.__dict__)
        serializer.is_valid(raise_exception=True)
        serializerResponse = serializer.data
        return Response(serializerResponse)

    def destroy(self,request,*args,**kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({})
